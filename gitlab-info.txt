Command line instructions


Git global setup

git config --global user.name "Dev Nath"
git config --global user.email "sdnath2020@gmail.com"

Create a new repository

git clone https://gitlab.com/Dev2020/DEV_177218_B69_Assignment_Four.git
cd DEV_177218_B69_Assignment_Four
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Dev2020/DEV_177218_B69_Assignment_Four.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/Dev2020/DEV_177218_B69_Assignment_Four.git
git push -u origin --all
git push -u origin --tags
Remove project