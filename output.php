<?php

class StudentInformation {

    protected $_stdID;
    protected $_stdName;
    protected $_stdDOB;
    protected $_stdGender;
    protected $_stdBanglaMark;
    protected $_stdEnglishMark;
    protected $_stdMathMark;
    protected $_stdICTMark;

    public function setFormData($_formInfo) {

        if(array_key_exists("ID",$_formInfo)){
            $this->_stdID = $_formInfo["ID"];
        }
        if(array_key_exists("Name",$_formInfo)){
            $this->_stdName = $_formInfo["Name"];
        }
        if(array_key_exists("DOB",$_formInfo)){
            $this->_stdDOB = $_formInfo["DOB"];
        }
        if(array_key_exists("Gender",$_formInfo)){
            $this->_stdGender = $_formInfo["Gender"];
        }
        if(array_key_exists("BanglaMark",$_formInfo)){
            $this->_stdBanglaMark = floatval($_formInfo["BanglaMark"]);
        }
        if(array_key_exists("EnglishMark",$_formInfo)){
            $this->_stdEnglishMark = floatval($_formInfo["EnglishMark"]);
        }
        if(array_key_exists("MathMark",$_formInfo)){
            $this->_stdMathMark = floatval($_formInfo["MathMark"]);
        }
        if(array_key_exists("ICTMark",$_formInfo)){
            $this->_stdICTMark = floatval($_formInfo["ICTMark"]);
        }

    }

    public function displayStudentID()
    {
        return $this->_stdID;
    }
    public function displayStudentName()
    {
        return $this->_stdName;
    }
    public function displayStudentDOB()
    {
        return $this->_stdDOB;
    }
    public function displayStudentGender()
    {
        return $this->_stdGender;
    }
    public function displayStudentBanglaMark()
    {
        return $this->_stdBanglaMark;
    }
    public function displayStudentEnglishMark()
    {
        return $this->_stdEnglishMark;
    }
    public function displayStudentMathMark()
    {
        return $this->_stdMathMark;
    }
    public function displayStudentICTMark()
    {
        return $this->_stdICTMark;
    }
    public function displayGradeFunction($_gradeFunction)
    {
        if($_gradeFunction<=100 && $_gradeFunction>=80){
            echo "<b>A<sup>+</sup></b>";
        }elseif ($_gradeFunction<=79 && $_gradeFunction>=70){
            echo "<b>A</b>";
        }elseif ($_gradeFunction<=69 && $_gradeFunction>=60){
            echo "<b>A<sup>-</sup></b>";
        }elseif ($_gradeFunction<=59 && $_gradeFunction>=50){
            echo "<b>B</b>";
        }elseif ($_gradeFunction<=49 && $_gradeFunction>=40){
            echo "<b>C</b>";
        }elseif ($_gradeFunction<=39 && $_gradeFunction>=33){
            echo "<b>D</b>";
        }else {
            echo "<b class=\"fGrade\">F</b>";
        }
    }
    public function displayFinalResult()
    {
        if($this->_stdBanglaMark>=33 && $this->_stdBanglaMark<=100 && $this->_stdEnglishMark>=33 && $this->_stdEnglishMark<=100 && $this->_stdMathMark>=33 && $this->_stdMathMark<=100 && $this->_stdICTMark>=33 && $this->_stdICTMark<=100){
            echo '<span class="pass">Pass</span>';
        }else{
            echo '<span class="fail">Fail</span>';
        }
    }

}

$objStudentInformation = new StudentInformation;

$objStudentInformation->setFormData($_POST);

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Information</title>
    <style>

        #InformationHolder {
            width: 750px;
            margin: 50px auto;
            padding:25px;
            background: #111111;
            color: #ffffff;
            font-size: 15px;
            font-weight: normal;
            line-height: 20px;
            letter-spacing: .5px;
            box-shadow: 5px 5px 5px rgba(0,0,0,.3);
            font-family: Arial;
        }
        p {
            margin: 0px;
        }
        #pageTitle {
            padding:15px 0;
        }
        #pageTitle h1 {
            padding: 0;
            margin: 0;
            text-align: center;
            font-size: 24px;
            font-weight: normal;
            line-height: 30px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-family: "Arial Black";
        }
        #stdInfo {
            padding: 30px 0;
        }
        #stdInfo table td {
            padding: 5px;
        }
        #markSheet {
            padding: 0 0 15px;
        }
        #markSheet table {

        }
        #markSheet table td {
            width: 33.33%;
            padding: 5px;
            text-align: center;
            border: 1px solid #ffffff;
        }
        #markSheet strong {
            display: block;
            padding: 5px;
            text-transform: uppercase;
        }
        .fGrade {
            color: red;
        }
        #finalResult {
            padding: 15px 0;
            font-size: 18px;
            font-weight: normal;
            line-height: 20px;
            letter-spacing: 2px;
            text-align: center;
            text-transform: uppercase;
            font-family: "Arial Black";
        }
        .pass {
            display: block;
            width: 120px;
            height: 40px;
            margin: 0 auto;
            line-height: 40px;
            background: green;
        }
        .fail {
            display: block;
            width: 120px;
            height: 40px;
            margin: 0 auto;
            line-height: 40px;
            background: red;
        }
        #goBack {
            padding: 15px 0;
        }
        #goBack a {
            display: block;
            text-decoration: none;
            text-transform: uppercase;
            width: 250px;
            height: 50px;
            line-height: 50px;
            letter-spacing: 2px;
            margin: 0 auto;
            text-align: center;
            font-size: 18px;
            font-weight: normal;
            font-family: "Arial Black";
            background: blue;
            color: #ffffff;
        }
    </style>
</head>
<body>

<div id="InformationHolder">
    <div id="pageTitle">
        <h1>Student Information</h1>
    </div>
    <div id="stdInfo">

        <table>
            <tr>
                <td>Student ID</td>
                <td>:</td>
                <td><?php echo $objStudentInformation->displayStudentID(); ?></td>
            </tr>
            <tr>
                <td>Student Name</td>
                <td>:</td>
                <td><?php echo $objStudentInformation->displayStudentName(); ?></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>:</td>
                <td><?php echo $objStudentInformation->displayStudentGender(); ?></td>
            </tr>
            <tr>
                <td>Date of Birth</td>
                <td>:</td>
                <td><?php echo $objStudentInformation->displayStudentDOB(); ?></td>
            </tr>
        </table>
    </div>

    <div id="markSheet">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td><strong>Subsect Name</strong></td>
                <td><strong>Obtained Mark</strong></td>
                <td><strong>Grade</strong></td>
            </tr>
            <tr>
                <td>Bangla</td>
                <td><?php echo $objStudentInformation->displayStudentBanglaMark(); ?></td>
                <td><?php echo $objStudentInformation->displayGradeFunction($objStudentInformation->displayStudentBanglaMark()); ?></td>
            </tr>
            <tr>
                <td>English</td>
                <td><?php echo $objStudentInformation->displayStudentEnglishMark(); ?></td>
                <td><?php echo $objStudentInformation->displayGradeFunction($objStudentInformation->displayStudentEnglishMark()); ?></td>
            </tr>
            <tr>
                <td>Math</td>
                <td><?php echo $objStudentInformation->displayStudentMathMark(); ?></td>
                <td><?php echo $objStudentInformation->displayGradeFunction($objStudentInformation->displayStudentMathMark()); ?></td>
            </tr>
            <tr>
                <td>ICT</td>
                <td><?php echo $objStudentInformation->displayStudentICTMark(); ?></td>
                <td><?php echo $objStudentInformation->displayGradeFunction($objStudentInformation->displayStudentICTMark()); ?></td>
            </tr>
        </table>
    </div>

    <div id="finalResult">
        <?php echo $objStudentInformation->displayFinalResult(); ?>
    </div>
    <div id="goBack">
        <a href="input.php">Input Another</a>
    </div>
</div>
</body>
</html>
