<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Input</title>
    <style>

        #InformationHolder {
            width: 750px;
            margin: 50px auto;
            padding:25px;
            background: #111111;
            color: #ffffff;
            font-size: 15px;
            font-weight: normal;
            line-height: 20px;
            letter-spacing: .5px;
            box-shadow: 5px 5px 5px rgba(0,0,0,.3);
            font-family: Arial;
        }
        p {
            margin: 0px;
        }
        #pageTitle {
            padding:15px 0;
        }
        #pageTitle h1 {
            padding: 0;
            margin: 0;
            text-align: center;
            font-size: 24px;
            font-weight: normal;
            line-height: 30px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-family: "Arial Black";
        }
        #stdInfo {
            padding: 30px 0;
        }
        #stdInfo table td {
            padding: 5px;
        }
        #InputMark {
            padding: 0 0 15px;
        }
        #InputMark table td {
            padding: 5px;
        }
        #submit {
            padding: 15px 0;
        }
        #submit input {
            display: block;
            text-transform: uppercase;
            width: 250px;
            height: 50px;
            line-height: 50px;
            letter-spacing: 2px;
            margin: 0 auto;
            text-align: center;
            font-size: 18px;
            font-weight: normal;
            font-family: "Arial Black";
            background: blue;
            color: #ffffff;
            cursor: pointer;
            border: none;
            outline: none;
        }

    </style>
</head>
<body>
<div id="InformationHolder">
    <form action="output.php" method="post" enctype="multipart/form-data">
        <div id="pageTitle">
            <h1>Student Information</h1>
        </div>

        <div id="stdInfo">

            <table>
                <tr>
                    <td>Student ID</td>
                    <td>:</td>
                    <td><input type="number" name="ID" style="padding: 5px 10px;width: 250px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
                <tr>
                    <td>Student Name</td>
                    <td>:</td>
                    <td><input type="text" name="Name" style="padding: 5px 10px;width: 250px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>:</td>
                    <td><input type="radio" name="Gender" value="Male"> Male <input type="radio" name="Gender" value="Female"> Female</td>
                </tr>
                <tr>
                    <td>Date of Birth</td>
                    <td>:</td>
                    <td><input type="date" name="DOB" style="padding: 5px 10px;width: 250px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
            </table>
        </div>
        <div id="InputMark">
            <table>
                <tr>
                    <td>Obtained Mark in Bangla</td>
                    <td>:</td>
                    <td><input type="number" name="BanglaMark"  min="0" max="100" style="padding: 5px 10px;width: 50px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
                <tr>
                    <td>Obtained Mark in English</td>
                    <td>:</td>
                    <td><input type="number" name="EnglishMark"  min="0" max="100" style="padding: 5px 10px;width: 50px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
                <tr>
                    <td>Obtained Mark in Math</td>
                    <td>:</td>
                    <td><input type="number" name="MathMark"  min="0" max="100" style="padding: 5px 10px;width: 50px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
                <tr>
                    <td>Obtained Mark in ICT</td>
                    <td>:</td>
                    <td><input type="number" name="ICTMark"  min="0" max="100" style="padding: 5px 10px;width: 50px;height: 15px;background: #fff;border: none;outline: none;"></td>
                </tr>
            </table>
        </div>
        <div id="submit">
            <input type="submit" value="Submit">
        </div>
    </form>
</div>
</body>
</html>
